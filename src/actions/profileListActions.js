import {
  ADD_PROFILE,
  CHANGE_PROFILE,
  FETCH_PROFILES,
  FILTER_PROFILES,
  FETCH_TECHNOLOGIES,
  FETCH_PROJECTS,
  FETCH_PROJECT,
  FETCH_TECHNOLOGY,
  FETCH_USERS,
  ADD_PROJECT,
  ADD_TECH,
  DELETE_PROJECT,
  DELETE_TECH,
  ADD_USER,
  UPDATE_USER,
  DELETE_USER,
  UPDATE_PROFILE,
  DELETE_PROFILE,
  KEEP_ME_SIGNED_IN,
  SET_USER
} from "../constants/profileListConstants";

import axios, { setTokenToAxios } from "../utils/axios";

export const addProfile = (newId, name) => ({
  type: ADD_PROFILE,
  newId,
  name
});

export const changeProfile = otherProfiles => ({
  type: CHANGE_PROFILE,
  otherProfiles
});

export const filteredProfiles = profiles => ({
  type: FILTER_PROFILES,
  profiles
});

export const setUserWithToken = User => {
  return dispatch => {
    setTokenToAxios(User.success.token);

    dispatch(setUser(User));
  };
};

export const setUser = User => {
  return {
    type: SET_USER,
    User
  };
};

export const fetchProfiles = () => {
  return dispatch => {
    axios
      .get("all-profiles")
      .then(res => dispatch({ type: FETCH_PROFILES, payload: res.data }));
  };
};

export const fetchTechnologies = () => {
  return dispatch => {
    axios
      .get("get-technologies")
      .then(res => dispatch({ type: FETCH_TECHNOLOGIES, payload: res.data }));
  };
};
export const fetchTechnology = id => {
  return dispatch => {
    axios
      .get(`technology/${id}`)
      .then(res => dispatch({ type: FETCH_TECHNOLOGY, payload: res.data }));
  };
};

export const fetchProjects = () => {
  return dispatch => {
    axios
      .get("get-projects")
      .then(res => dispatch({ type: FETCH_PROJECTS, payload: res.data }));
  };
};

export const fetchProject = projectID => {
  return dispatch => {
    axios
      .get(`project/${projectID}`)
      .then(res => dispatch({ type: FETCH_PROJECT, payload: res.data }));
  };
};

export const createProfile = profile => {
  return dispatch => {
    axios.post("store-profile", { ...profile }).then(res => {
      dispatch({ type: ADD_PROFILE, payload: res.data });
    });
  };
};

export const createProject = project => {
  return dispatch => {
    return axios.post("store-project", { ...project }).then(res => {
      dispatch({ type: ADD_PROJECT, payload: res.data.project });
      return res.data.project;
    });
  };
};

export const deleteProject = project => {
  return dispatch => {
    axios
      .delete(`destroy-project/${project}`)
      .then(res => dispatch({ type: DELETE_PROJECT, payload: project }));
  };
};

export const createTech = tech => {
  return dispatch => {
    return axios.post(`store-technology`, { ...tech }).then(res => {
      dispatch({ type: ADD_TECH, payload: res.data.technology });
      return res.data.technology;
    });
  };
};

export const deleteTech = tech => {
  return dispatch => {
    axios
      .delete(`destroy-technology/${tech}`)
      .then(res => dispatch({ type: DELETE_TECH, payload: tech }));
  };
};

export const createUser = user => {
  return dispatch => {
    axios
      .post(`register`, { ...user })
      .then(res => dispatch({ type: ADD_USER, payload: res.data.user }));
  };
};

export const updateUser = (user_id, user) => {
  return dispatch => {
    axios.put(`update-user/${user_id}`, { ...user }).then(res => {
      dispatch({ type: UPDATE_USER, payload: res.data.user });
    });
  };
};

export const deleteUser = user => {
  return dispatch => {
    axios
      .delete(`destroy-user/${user}`)
      .then(res => dispatch({ type: DELETE_USER, payload: user }));
  };
};

export const getUsers = () => {
  return dispatch => {
    axios
      .get("users")
      .then(res => dispatch({ type: FETCH_USERS, payload: res.data.users }));
  };
};

export const updateProfile = (id, profile) => {
  return dispatch => {
    axios.put(`update-profile/${id}`, { ...profile }).then(res => {
      dispatch({ type: UPDATE_PROFILE, payload: res });
    });
  };
};
export const deleteProfile = profileID => {
  return dispatch => {
    axios.delete(`destroy-profile/${profileID}`, { profileID }).then(res => {
      dispatch({ type: DELETE_PROFILE, payload: profileID });
    });
  };
};

export const changeKeepMeSignedIn = () => {
  return {
    type: KEEP_ME_SIGNED_IN,
    payload: {}
  };
};
