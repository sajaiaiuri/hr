import * as yup from "yup";

export const validateAddInputs = (name, email, password, c_password) => {
  let schema = yup.object().shape({
    name: yup.string().required(),
    email: yup
      .string()
      .email("Please enter a valid email")
      .required(),
    password: yup
      .string()
      .required()
      .test("password", "Password must be 8 characters", function(value) {
        if (!!value) {
          const schema = yup.string().min(8);
          return schema.isValidSync(value);
        }
        return true;
      }),
    c_password: yup
      .string()
      .required()
      .oneOf([yup.ref("password"), null], "Passwords do not match")
  });
  return schema.validate(
    {
      name: name,
      email: email,
      password: password,
      c_password: c_password
    },
    {
      abortEarly: false
    }
  );
};

export const validateEditInputs = (name, email, password, c_password) => {
  let schema = yup.object().shape({
    name: yup.string().required(),
    email: yup
      .string()
      .email("Please enter a valid email")
      .required(),
    password: yup
      .string()
      .notRequired()
      .test("password", "Password must be 8 characters", function(value) {
        if (!!value) {
          const schema = yup.string().min(8);
          return schema.isValidSync(value);
        }
        return true;
      }),
    c_password: yup
      .string()
      .oneOf([yup.ref("password"), null], "Passwords do not match")
  });
  return schema.validate(
    {
      name: name,
      email: email,
      password: password,
      c_password: c_password
    },
    {
      abortEarly: false
    }
  );
};

export const validateAddProfile = (
  name,
  phone,
  position,
  profile,
  projects,
  technologies
) => {
  let schema = yup.object().shape({
    name: yup.string().required(),
    phone: yup.string().required(),
    position: yup.string().required(),
    profile: yup.string().required(),
    projects: yup.array().required(),
    technologies: yup.array().required()
  });
  return schema.validate(
    {
      name: name,
      phone: phone,
      position: position,
      profile: profile,
      projects: projects,
      technologies: technologies
    },
    {
      abortEarly: false
    }
  );
};
