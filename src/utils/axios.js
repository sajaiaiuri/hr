import axios from "axios";
import * as requests from "../constants/requests";

// create axios custom instance with custom config
const customAxios = axios.create({
  baseURL: requests.baseURL,
  headers: {
    common: {
      Authorization: false,
      Accept: "application/json",
      "Content-Type": "application/json"
    }
  }
});

// enable global reponse handling
customAxios.interceptors.response.use(
  function(response) {
    // pass response without change
    return response;
  },
  function(error) {
    // get error info
    let statusCode = error.response.status;

    switch (statusCode) {
      case 401:
        // token is invalid, so there is need to reload page
        localStorage.removeItem("User");
        window.location = "/";
        // console.log(error.response.data.error[0]);
        break;
      case 400:
        if (error.response.data.error[Object.keys(error.response.data.error)]) {
          alert(
            error.response.data.error[Object.keys(error.response.data.error)][0]
          );
        } else {
          alert(error.response.data.error);
        }

      default:
        alert("Sorry, Something Went Wrong!");
    }
    // pass error without change
    return Promise.reject(error);
  }
);

// define function which updates axios authorization header
export const setTokenToAxios = token => {
  // console.log("[axios.js][setTokenToAxios]");
  customAxios.defaults.headers.common["Authorization"] = "Bearer " + token; // That"s correct way of token: "Bearer ..."
};

export default customAxios;
