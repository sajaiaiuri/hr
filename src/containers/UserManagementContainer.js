import { connect } from "react-redux";
import {
  createUser,
  updateUser,
  deleteUser
} from "../actions/profileListActions";
import UserManagement from "../components/UserManagement/UserManagement";

const mapStateToProps = state => {
  return {
    users: state.profileListReducer.users,
    User: state.profileListReducer.User
  };
};

const mapDispatchToProps = dispatch => ({
  createUser: user => dispatch(createUser(user)),
  updateUser: (user_id, user) => dispatch(updateUser(user_id, user)),
  deleteUser: (user, token) => dispatch(deleteUser(user, token))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserManagement);
