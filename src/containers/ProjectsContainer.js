import { connect } from "react-redux";
import Projects from "../components/Home/Projects/Projects";
import { createProject, deleteProject } from "../actions/profileListActions";

const mapStateToProps = state => {
  return {
    projects: state.profileListReducer.projects,
    user: state.profileListReducer.User
  };
};

const mapDispatchToProps = dispatch => ({
  createProject: project => dispatch(createProject(project)),
  deleteProject: project => dispatch(deleteProject(project))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Projects);
