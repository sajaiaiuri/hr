import { connect } from "react-redux";
import UserProfile from "../components/UserProfile/Userprofile";
import { updateProfile, deleteProfile } from "../actions/profileListActions";

const mapStateToProps = (state, ownProps) => ({
  profile: state.profileListReducer.profiles
    ? state.profileListReducer.profiles.filter(
        profile => profile.id === Number(ownProps.match.params.id)
      )[0]
    : [],
  technologies: state.profileListReducer.technologies,
  projects: state.profileListReducer.projects,
  user: state.profileListReducer.User
});

const mapDispatchToProps = dispatch => {
  return {
    updateProfile: (id, profile) => dispatch(updateProfile(id, profile)),
    deleteProfile: id => {
      dispatch(deleteProfile(id));
    }
  };
};

const SingleProfileContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserProfile);

export default SingleProfileContainer;
