import { connect } from "react-redux";
import SingleTechnology from "../components/Technologies/single";
import { fetchTechnology } from "../actions/profileListActions";

const mapStateToProps = state => {
  return {
    choosenTechnology: state.profileListReducer.choosenTechnology
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchTechnology: id => dispatch(fetchTechnology(id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SingleTechnology);
