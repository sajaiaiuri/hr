import { connect } from "react-redux";
import technologies from "../components/Technologies/technologies";
import { createTech, deleteTech } from "../actions/profileListActions";

const mapStateToProps = state => {
  return {
    technologies: state.profileListReducer.technologies,
    user: state.profileListReducer.User
  };
};
const mapDispatchToProps = dispatch => ({
  createTech: tech => dispatch(createTech(tech)),
  deleteTech: tech => dispatch(deleteTech(tech))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(technologies);
