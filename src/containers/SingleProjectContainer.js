import { connect } from "react-redux";
import { fetchProject } from "../actions/profileListActions";
import SingleProject from "../components/Home/Projects/single";

const mapStateToProps = state => {
  return {
    choosenProject: state.profileListReducer.choosenProject
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getProject: projectID => dispatch(fetchProject(projectID))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SingleProject);
