import React, { Component } from "react";
// import Header from "../Header/Header";
import Sidebar from "../../containers/SidebarContainer";
import { Route } from "react-router-dom";
import ProjectsContainer from "../../containers/ProjectsContainer";
import CalendarView from "../Calendar/Calendar";
import BlackList from "../BlackList/BlackList";
import SingleProfileContainer from "../../containers/SingleProfile";
import ProfileListContainer from "../../containers/ProfileListContainer";
import TechnologyContainer from "../../containers/TechnologyContainer";
import { connect } from "react-redux";
import {
  fetchProfiles,
  fetchTechnologies,
  fetchProjects,
  getUsers
} from "../../actions/profileListActions";
import AnalyticsContainer from "../../containers/AnalyticsContainer";
import SingleTechnologyContainer from "../../containers/SingleTechnologyContainer";
import SingleProjectContainer from "../../containers/SingleProjectContainer";
import UserManagementContainer from "../../containers/UserManagementContainer";

class ProtectedContainer extends Component {
  state = {};
  componentWillMount() {
    this.props.getProfiles();
    this.props.getTechnologies();
    this.props.getProjects();
    this.props.getUsers();
  }
  render() {
    return (
      <div>
        <div className="mainContent">
          <Sidebar />
          <Route
            path="/home/profile_list"
            exact
            component={ProfileListContainer}
          />
          <Route path="/home/projects" exact component={ProjectsContainer} />
          <Route path="/home/analytics" exact component={AnalyticsContainer} />
          <Route path="/home/calendar" exact component={CalendarView} />
          <Route path="/home/black_list" exact component={BlackList} />
          <Route
            path="/home/technologies"
            exact
            component={TechnologyContainer}
          />
          <Route
            path="/home/projects/:id"
            exact
            component={SingleProjectContainer}
          />
          <Route
            path="/home/technologies/:id"
            exact
            component={SingleTechnologyContainer}
          />
          <Route
            path="/home/user_management"
            exact
            component={UserManagementContainer}
          />
          <Route
            path="/home/profile/:id"
            exact
            component={SingleProfileContainer}
          />
          <Route
            path="/home/technologies/profile/:id"
            exact
            component={SingleProfileContainer}
          />
          <Route
            path="/home/projects/profile/:id"
            exact
            component={SingleProfileContainer}
          />
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  getProfiles: () => dispatch(fetchProfiles()),
  getTechnologies: () => dispatch(fetchTechnologies()),
  getProjects: () => dispatch(fetchProjects()),
  getUsers: () => dispatch(getUsers())
});

export default connect(
  null,
  mapDispatchToProps
)(ProtectedContainer);
