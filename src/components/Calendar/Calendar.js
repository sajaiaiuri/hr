import React, { Component } from "react";
class CalendarView extends Component {
  render() {
    return (
      <div
        style={{
          width: "Calc(100% - 250px)",
          margin: "40px auto"
        }}
      >
        <iframe
          title="calendar"
          src="https://calendar.google.com/calendar/embed?src=sajaiaiuri%40gmail.com&ctz=Asia%2FTbilisi"
          style={{ margin: "20px auto" }}
          width="100%"
          height="700"
          frameBorder="0"
          scrolling="no"
        />
      </div>
    );
  }
}

export default CalendarView;
