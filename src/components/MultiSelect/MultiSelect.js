import React, { Component } from "react";
import PropTypes from "prop-types";
import { components } from "react-select";
import Creatable from "react-select/creatable";
import { createTech, createProject } from "../../actions/profileListActions";
import { connect } from "react-redux";
const Option = props => (
  <div>
    <components.Option {...props}>
      <label>{props.label}</label>
    </components.Option>
  </div>
);

const MultiValue = props => (
  <components.MultiValue {...props}>
    <span>{props.data.label}</span>
  </components.MultiValue>
);

const customStyles = error => {
  return {
    option: provided => ({
      ...provided,
      borderBottom: "1px dotted pink",
      width: "100%",
      display: "flex"
    }),
    container: provided => ({
      ...provided,
      width: "100%",
      margin: "3px 12px",
      border: error ? "1px solid red" : "none",
      background: "rgb(230, 225, 225,0.463)",
      borderRadius: "3px",
      color: "#494949",
      display: "inline-block"
    }),
    control: () => ({}),
    placeholder: () => ({
      color: error ? "red" : ""
    }),
    indicatorsContainer: () => ({
      display: "none"
    }),

    singleValue: provided => {
      const transition = "opacity 300ms";
      return { ...provided, transition };
    }
  };
};

class MultiSelect extends Component {
  state = {
    tmpOption: null
  };
  static propTypes = {
    options: PropTypes.arrayOf(
      PropTypes.shape({
        value: PropTypes.node,
        label: PropTypes.label
      })
    ).isRequired,
    onChangeCallback: PropTypes.func.isRequired
  };

  static defaultProps = {
    options: []
  };
  onCreateOption = value => {
    if (this.props.type === "technologies") {
      this.props
        .createTech({
          title: value,
          author_id: this.props.User.user.id
        })
        .then(res => {
          this.props.setTechnologies(res.id);
        });
    } else if (this.props.type === "projects") {
      this.props
        .createProject({
          title: value,
          author_id: this.props.User.user.id
        })
        .then(res => {
          this.props.setProjects(res.id);
        });
    }
  };

  render() {
    const { options, onChangeCallback, ...otherProps } = this.props;
    const tmp = [];
    this.props.selected.forEach(element => {
      tmp.push(options.find(x => x.value === element));
    });
    return (
      <Creatable
        onCreateOption={this.onCreateOption}
        value={this.state.tmpOption ? [...tmp, this.state.tmpOption] : tmp}
        placeholder={this.props.error}
        isRequired
        styles={customStyles(this.props.error)}
        closeMenuOnSelect={false}
        isMulti
        components={{ Option, MultiValue }}
        options={
          this.state.tmpOption ? [...options, this.state.tmpOption] : options
        }
        hideSelectedOptions={true}
        backspaceRemovesValue={true}
        onChange={e => onChangeCallback(e)}
        {...otherProps}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    User: state.profileListReducer.User
  };
};

const mapDispatchToProps = dispatch => {
  return {
    createTech: tech => dispatch(createTech(tech)),
    createProject: project => dispatch(createProject(project))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MultiSelect);
