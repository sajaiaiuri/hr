import React, { Component } from "react";
import Backdrop from "../Backdrop/Backdrop";
import Modal from "../Modal/Modal";
import styles from "./UserProfile.module.css";
import Button from "../UI/button/Button";
import PermissionGuard from "../HOC/PermissionGuard";

class UserProfile extends Component {
  state = {
    creating: false,
    name: "",
    phone: "",
    position: "",
    profile: "",
    portfolio: "",
    comment: "",
    english: "",
    salary: "",
    status: "",
    source: "",
    recommender: "",
    chosenProjects: [],
    chosenTechnologies: [],
    errors: {}
  };
  modalCancelHandler = () => {
    this.setState({ creating: false });
  };
  handleMultiSelect = (category, data) => {
    if (category === "technologies") {
      this.setState({
        chosenTechnologies: data
      });
    }
    if (category === "projects") {
      this.setState({ chosenProjects: data });
    }
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value, errors: {} });
  };
  handleSubmit = e => {
    e.preventDefault();
    const id = this.props.profile.id;
    const profile = {
      updater_id: this.props.user.user.id,
      name: this.state.name,
      phone: this.state.phone,
      position: this.state.position,
      profile: this.state.profile,
      portfolio: this.state.portfolio,
      comment: this.state.comment,
      english: this.state.english,
      salary: this.state.salary,
      status: this.state.status,
      source: this.state.source,
      projects: this.state.chosenProjects,
      technologies: this.state.chosenTechnologies
    };
    this.props.updateProfile(id, profile);
    this.setState({ creating: false });
  };
  handleDelete = id => {
    this.props.deleteProfile(id);
    this.props.history.push("/home/profile_list");
  };
  render() {
    const profile = this.props.profile;
    return (
      <>
        {profile ? (
          <div className={styles.row}>
            {this.state.creating && (
              <>
                <Backdrop />
                <Modal
                  title="Edit Candidate"
                  submitButtontext="Edit"
                  canCancel
                  canConfirm
                  onCancel={this.modalCancelHandler}
                  onConfirm={this.handleSubmit}
                  fields={[
                    {
                      name: "name",
                      type: "text",
                      label: "Name",
                      value: this.state.name
                    },
                    {
                      name: "phone",
                      type: "text",
                      label: "Phone",
                      value: this.state.phone
                    },
                    {
                      name: "position",
                      type: "text",
                      label: "Position",
                      value: this.state.position
                    },
                    {
                      name: "portfolio",
                      type: "text",
                      label: "Portfolio",
                      value: this.state.portfolio
                    },
                    {
                      name: "comment",
                      type: "text",
                      label: "Comment",
                      value: this.state.comment
                    },
                    {
                      name: "salary",
                      type: "text",
                      label: "Salary Expectation",
                      value: this.state.salary
                    },
                    {
                      name: "profile",
                      type: "text",
                      label: "Profile",
                      value: this.state.profile
                    },
                    {
                      name: "recommender",
                      type: "text",
                      label: "Recommender",
                      value: this.state.recommender
                    },
                    {
                      name: "english",
                      type: "select",
                      label: "English",
                      options: [
                        { value: "no english", label: "no english" },
                        { value: "good", label: "good" },
                        { value: "fluent", label: "fluent" }
                      ]
                    },
                    {
                      name: "status",
                      type: "select",
                      label: "Status",
                      options: [
                        {
                          value: "Wrote on LinkedIn",
                          label: "Wrote on LinkedIn"
                        },
                        { value: "refused", label: "Refused" },
                        { value: "interested", label: "Interested" },
                        { value: "rejected", label: "Rejected" },
                        { value: "shortlisted", label: "Shortlisted" },
                        { value: "hired", label: "Hired" },
                        { value: "black_list", label: "Blacklisted" }
                      ]
                    },
                    {
                      name: "source",
                      type: "select",
                      label: "Source",
                      options: [
                        { value: "LinkedIn", label: "LinkedIn" },
                        { value: "Refference", label: "Refference" },
                        { value: "Job Post", label: "Job Post" }
                      ]
                    },
                    {
                      name: "technologies",
                      type: "multiSelect",
                      label: "Technologies",
                      options: this.props.technologies.map(item => {
                        return { value: item.id, label: item.title };
                      }),
                      selected: this.state.chosenTechnologies
                    },
                    {
                      name: "projects",
                      type: "multiSelect",
                      label: "Projects",
                      options: this.props.projects.map(item => {
                        return { value: item.id, label: item.title };
                      }),
                      selected: this.state.chosenProjects
                    }
                  ]}
                  onChange={this.handleChange}
                  handleMultiSelect={this.handleMultiSelect}
                  createProfile={this.createProfile}
                  errors={this.state.errors}
                  candidate={{
                    name: this.state.name,
                    phone: this.state.phone,
                    position: this.state.position,
                    portfolio: this.state.portfolio,
                    profile: this.state.profile,
                    comment: this.state.comment,
                    english: this.state.english,
                    salary: this.state.salary,
                    source: this.state.source,
                    status: this.state.status,
                    black_list: this.state.black_list
                  }}
                />
              </>
            )}
            <div className={styles.col4}>
              <div className={styles.wrap}>
                <div className={styles.box}>
                  <span className={styles.userInfoTitle}>Name:</span>
                  {profile.name}
                </div>
                <div className={styles.box}>
                  <span className={styles.userInfoTitle}>Phone:</span>
                  {profile.phone}
                </div>
                <div className={styles.box}>
                  <span className={styles.userInfoTitle}>Position:</span>
                  {profile.position}
                </div>
                <div className={styles.box}>
                  <span className={styles.userInfoTitle}>Portfolio:</span>
                  {profile.portfolio}
                </div>
                <div className={styles.box}>
                  <span className={styles.userInfoTitle}>Source:</span>
                  {profile.source}
                </div>
                <div className={styles.box}>
                  <span className={styles.userInfoTitle}>English:</span>
                  {profile.english}
                </div>
                <div className={styles.box}>
                  <span className={styles.userInfoTitle}>Salary:</span>
                  {profile.salary}
                </div>
                <div className={styles.box}>
                  <span className={styles.userInfoTitle}>Status:</span>
                  {profile.status}
                </div>
                <PermissionGuard roles={["admin", "manager"]}>
                  <div className={styles.btnContainer}>
                    <div
                      className={styles.edit}
                      onClick={() => {
                        this.setState({
                          creating: true,
                          name: profile.name,
                          phone: profile.phone,
                          position: profile.position,
                          profile: profile.profile,
                          portfolio: profile.portfolio,
                          comment: profile.comment,
                          english: profile.english,
                          salary: profile.salary,
                          status: profile.status,
                          source: profile.source,
                          black_list: profile.black_list,
                          chosenProjects: this.props.profile.projects.map(
                            item => item.id
                          ),
                          chosenTechnologies: this.props.profile.technologies.map(
                            item => item.id
                          )
                        });
                      }}
                    >
                      <Button
                        value="Edit profile"
                        buttonClass="change"
                        buttonType="submit"
                      />
                    </div>
                    <div
                      className={styles.delete}
                      onClick={() => {
                        this.handleDelete(profile.id);
                      }}
                    >
                      <Button
                        value="Delete Profile"
                        buttonClass="delete"
                        buttonType="submit"
                      />
                    </div>
                  </div>
                </PermissionGuard>
              </div>
            </div>
            {/* </div> */}
            <div className={styles.col6}>
              <div className={styles.wrap}>
                {/* Projects */}
                <div className={styles.box}>
                  <h3>Projects</h3>
                  <div className={styles.userInfoItem}>
                    <div className={styles.listContainer2}>
                      {profile.projects.map(item => (
                        <span key={item.id} className={styles.listItem}>
                          #{item.title}
                        </span>
                      ))}
                    </div>
                  </div>
                </div>

                {/* Technologies */}
                <div className={styles.box}>
                  <h3>Technologies</h3>
                  <div className={styles.userInfoItem}>
                    <div className={styles.listContainer}>
                      {profile.technologies.map(item => (
                        <span key={item.id} className={styles.listItem}>
                          #{item.title}
                        </span>
                      ))}
                    </div>
                  </div>
                </div>

                {/* Comment */}
                <div className={styles.box}>
                  <span className={styles.userInfoTitle}>Comment:</span>
                  {profile.comment}
                </div>
              </div>
            </div>
          </div>
        ) : null}
      </>
    );
  }
}

export default UserProfile;
