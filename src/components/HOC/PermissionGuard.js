import React from "react";
import { connect } from "react-redux";

const PermissionGuard = props => {
  const { children, roles, userRole } = props;
  const hasAccess = roles.includes(userRole);
  return hasAccess ? children : <div />;
};
const mapStateToProps = state => {
  return {
    userRole: state.profileListReducer.User.user
      ? state.profileListReducer.User.user.role
      : ""
  };
};

export default connect(mapStateToProps)(PermissionGuard);
