import React, { Component } from "react";
import styles from "./UserManagement.module.css";
import editImg from "../../images/editIconGray.png";
import deleteImg from "../../images/deleteIconRed.png";
import Backdrop from "../Backdrop/Backdrop";
import Modal from "../Modal/Modal";
import * as validation from "../../utils/validation";

class UserManagement extends Component {
  state = {
    editing: false,
    add: {
      name: "",
      email: "",
      password: "",
      c_password: ""
    },
    user: {
      name: "",
      email: "",
      password: "",
      c_password: "",
      role: ""
    },
    editingUserId: "",
    addErrors: {},
    editErrors: {}
  };

  handleAddChange = e => {
    this.setState({
      add: { ...this.state.add, [e.target.name]: e.target.value },
      addErrors: {}
    });
  };

  handleAddSubmit = e => {
    e.preventDefault();
    validation
      .validateAddInputs(
        this.state.add.name,
        this.state.add.email,
        this.state.add.password,
        this.state.add.c_password
      )
      .then(() => {
        this.props.createUser({ ...this.state.add });
        this.setState({
          editing: false,
          add: { name: "", email: "", password: "", c_password: "" }
        });
      })
      .catch(err => {
        err.inner.map(item => {
          this.setState(currentState => ({
            addErrors: {
              ...currentState.addErrors,
              [item.path]: item.message
            },
            add: { ...currentState.add, [item.path]: "" }
          }));
          return 1;
        });
      });
  };

  addUser = () => {
    return (
      <>
        <div className={styles.addUserModalContainer}>
          <Modal
            title="Add User"
            submitButtontext="Add"
            canConfirm
            onCancel={this.modalCancelHandler}
            onConfirm={this.handleAddSubmit}
            fields={[
              {
                name: "name",
                type: "text",
                label: "Name"
              },
              {
                name: "email",
                type: "email",
                label: "email"
              },
              {
                name: "password",
                type: "password",
                label: "password"
              },
              {
                name: "c_password",
                type: "password",
                label: "Confirm Password"
              }
            ]}
            onChange={this.handleAddChange}
            handleMultiSelect={{}}
            errors={this.state.addErrors}
            candidate={{
              name: this.state.add.name,
              email: this.state.add.email,
              password: this.state.add.password,
              c_password: this.state.add.c_password
            }}
          />
        </div>
      </>
    );
  };

  handleEditChange = e => {
    this.setState({
      editErrors: {},
      user: {
        ...this.state.user,
        [e.target.name]: e.target.value
      }
    });
  };
  modalCancelHandler = () => {
    this.setState({
      editing: false,
      editErrors: {},
      user: {
        name: "",
        email: "",
        password: "",
        c_password: "",
        role: ""
      }
    });
  };

  modalConfirmHandler = e => {
    e.preventDefault();
    validation
      .validateEditInputs(
        this.state.user.name,
        this.state.user.email,
        this.state.user.password,
        this.state.user.c_password
      )
      .then(() => {
        this.props.updateUser(this.state.editingUserId, this.state.user);
        this.setState({ editing: false });
      })
      .catch(err => {
        err.inner.map(item => {
          this.setState(currentState => ({
            editErrors: {
              ...currentState.editErrors,
              [item.path]: item.message
            },
            user: { ...currentState.user, [item.path]: "" }
          }));
          return 1;
        });
      });
    this.setState({ editErrors: {} });
  };
  editUserModal = () => {
    return (
      <div className={styles.editUserModal}>
        <Modal
          title="Edit User"
          submitButtontext="Edit"
          canCancel
          canConfirm
          onCancel={this.modalCancelHandler}
          onConfirm={this.modalConfirmHandler}
          fields={[
            {
              name: "name",
              type: "text",
              label: "Name"
            },
            {
              name: "email",
              type: "email",
              label: "email"
            },
            {
              name: "password",
              type: "password",
              label: "password"
            },
            {
              name: "c_password",
              type: "password",
              label: "Confirm Password"
            },
            {
              name: "role",
              type: "select",
              label: "Role",
              options: [
                { value: "viewer", label: "viewer" },
                { value: "manager", label: "manager" },
                { value: "admin", label: "admin" }
              ]
            }
          ]}
          onChange={this.handleEditChange}
          handleMultiSelect={{}}
          errors={this.state.editErrors}
          candidate={{
            name: this.state.user.name,
            email: this.state.user.email,
            password: this.state.user.password,
            role: this.state.user.role
          }}
        />
      </div>
    );
  };
  render() {
    return (
      <div className={styles.container}>
        {this.addUser()}
        <table>
          <thead>
            <tr className={styles.headerRow}>
              <th>Username</th>
              <th>Role</th>
            </tr>
          </thead>
          <tbody>
            {this.props.users &&
              this.props.users.map(user => {
                return (
                  <tr key={user.id}>
                    <td>{user.email}</td>
                    <td>
                      {user.role}
                      <div className={styles.tableButtons}>
                        <button
                          className={styles.editUser}
                          onClick={() => {
                            this.setState(currentState => {
                              return {
                                errors: {},
                                user: {
                                  ...this.state.user,
                                  name: user.name,
                                  email: user.email,
                                  role: user.role
                                },
                                editingUserId: user.id,
                                editing: !currentState.editing
                              };
                            });
                          }}
                        >
                          <img
                            className={styles.icon}
                            src={editImg}
                            alt="icon"
                          />
                        </button>
                        <button
                          className={styles.deleteUser}
                          onClick={() =>
                            this.props.deleteUser(
                              user.id,
                              this.props.User.token
                            )
                          }
                        >
                          <img
                            className={styles.icon}
                            src={deleteImg}
                            alt="icon"
                          />
                        </button>
                      </div>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
        {this.state.editing && <Backdrop />}
        {this.state.editing && this.editUserModal()}
      </div>
    );
  }
}

export default UserManagement;
