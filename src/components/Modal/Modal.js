import React from "react";
import classes from "./Modal.module.css";
import MultiSelect from "../MultiSelect/MultiSelect";

const modal = props => {
  return (
    <div className={classes.modal}>
      <header className={classes.modal_header}>
        <h1>{props.title}</h1>
      </header>
      <section className={classes.modal_content}>
        <form onSubmit={props.onConfirm}>
          {props.fields.map(item => {
            if (item.type === "select") {
              const content = item.options ? (
                <select
                  name={item.name}
                  onChange={props.onChange}
                  key={item.name}
                >
                  {item.options.map(itm => {
                    return (
                      <option
                        defaultValue={
                          props.candidate[item.name] === itm.value
                            ? props.candidate[item.name]
                            : undefined
                        }
                        value={itm.value}
                        key={itm.label}
                      >
                        {props.candidate[itm.label] === itm.label
                          ? props.candidate[itm.label]
                          : itm.label}
                      </option>
                    );
                  })}
                </select>
              ) : (
                undefined
              );
              return (
                <div className={classes.selectBox} key={item.name}>
                  <div className={classes.modalForm}>
                    <label htmlFor={item.name}>{item.label}</label>
                    {content}
                  </div>
                </div>
              );
            } else if (item.type === "multiSelect") {
              return (
                <div className={classes.multiSelectBox} key={item.name}>
                  <div className={classes.modalForm}>
                    <label htmlFor={item.name}>{item.label}</label>
                    <div style={{ display: "block" }}>
                      <MultiSelect
                        setTechnologies={props.setTechnologies}
                        setProjects={props.setProjects}
                        type={item.name}
                        selected={item.selected}
                        default="Choose"
                        error={props.errors[item.name]}
                        options={item.options}
                        onChangeCallback={response => {
                          if (response) {
                            props.handleMultiSelect(
                              item.name,
                              response.map(item => item.value)
                            );
                          } else {
                            props.handleMultiSelect(item.name, []);
                          }
                        }}
                      />
                    </div>
                  </div>
                </div>
              );
            } else
              return (
                <div className={classes.pairBox} key={item.name}>
                  <div className={classes.modalForm}>
                    <label htmlFor={item.name}>{item.label}</label>
                    <input
                      className={props.errors[item.name] ? classes.error : ""}
                      placeholder={props.errors[item.name]}
                      type={item.type}
                      name={item.name}
                      value={
                        props.candidate ? props.candidate[item.name] : undefined
                      }
                      onChange={props.onChange}
                    />
                  </div>
                </div>
              );
          })}
          <button type="submit" />
        </form>
        <section className={classes.modal_actions}>
          {props.canCancel && (
            <button className={classes.modal_btn} onClick={props.onCancel}>
              Cancel
            </button>
          )}
          {props.canConfirm && (
            <button className={classes.modal_btn} onClick={props.onConfirm}>
              {props.submitButtontext}
            </button>
          )}
        </section>
      </section>
    </div>
  );
};

export default modal;
