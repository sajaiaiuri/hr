import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import styles from "./Sidebar.module.css";
import menuIcon from "../../images/menuIcon.png";
import profilesIcon from "../../images/profilesIcon.png";
import projectsIcon from "../../images/projectsIcon.png";
import analyticsIcon from "../../images/analyticsIcon.png";
import calendarIcon from "../../images/calendarIcon.png";
import TimeSvg from "./svg/Time.js";
import userManagementIcon from "../../images/userManagementIcon.png";
import technologiesIcon from "../../images/technologiesIcon.png";
import menuIconBlue from "../../images/menuIconBlue.png";
import profilesIconBlue from "../../images/profilesIconBlue.png";
import projectsIconBlue from "../../images/projectsIconBlue.png";
import analyticsIconBlue from "../../images/analyticsIconBlue.png";
import calendarIconBlue from "../../images/calendarIconBlue.png";
import userManagementIconBlue from "../../images/userManagementIconBlue.png";
import technologiesIconBlue from "../../images/technologiesIconBlue.png";
import auth from "../../auth/auth";
import LogOutSvg from "./svg/LogOutSvg";
import CloseSvg from "./svg/CloseSvg";
import PermissionGuard from "../HOC/PermissionGuard";

export class Sidebar extends Component {
  state = {
    style: false,
    open: JSON.parse(localStorage.getItem("sidebarState")),
    sidebarItems: [
      {
        title: "Profile List",
        path: "profile_list",
        icon: profilesIcon,
        iconBlue: profilesIconBlue
      },
      {
        title: "Projects",
        path: "projects",
        icon: projectsIcon,
        iconBlue: projectsIconBlue
      },
      {
        title: "Technologies",
        path: "technologies",
        icon: technologiesIcon,
        iconBlue: technologiesIconBlue
      },
      {
        title: "analytics",
        path: "analytics",
        icon: analyticsIcon,
        iconBlue: analyticsIconBlue
      },
      {
        title: "Calendar",
        path: "calendar",
        icon: calendarIcon,
        iconBlue: calendarIconBlue
      },

      {
        title: "User Management",
        path: "user_management",
        icon: userManagementIcon,
        iconBlue: userManagementIconBlue
      }
    ]
  };
  handleClick = () => {
    this.setState({ open: !this.state.open });
    localStorage.setItem("sidebarState", !this.state.open);
  };
  changeStyle = () => {
    this.setState(state => {
      return {
        style: !state.style
      };
    });
  };
  generateContent() {
    const sidebarStyles = this.state.open ? styles.sidebarOpen : styles.sidebar;
    const daynight = this.state.style ? styles.daydiv : styles.nightdiv;
    const daynighttext = this.state.style ? styles.daytext : styles.nighttext;
    const isVisible = this.state.open ? styles.visible : styles.invisible;

    let sidebarItems = this.state.sidebarItems.map(item => {
      let itemClass = styles.itemClass;
      let itemIcon = this.state.style ? item.iconBlue : item.icon;
      if (window.location.pathname === "/home/" + item.path) {
        itemClass = styles.chosen + " " + styles.itemClass;
        // itemIcon = item.iconBlue;
      }
      if (item.path === "user_management") {
        return (
          <PermissionGuard roles={["admin", "manager"]} key={item.path}>
            <li key={item.path} className={itemClass}>
              <span className={styles.tooltip}>{item.title}</span>
              <Link to={`/home/${item.path}`}>
                <button className={styles.menuBtn}>
                  <img
                    src={itemIcon}
                    className={styles.sidebarIcon}
                    alt={item.title}
                  />
                </button>
                <span className={`${daynighttext} ${isVisible}`}>
                  {item.title}
                </span>
              </Link>
            </li>
          </PermissionGuard>
        );
      }
      return (
        <li key={item.path} className={itemClass}>
          <span className={styles.tooltip}>{item.title}</span>
          <Link to={`/home/${item.path}`}>
            <button className={styles.menuBtn}>
              <img
                src={itemIcon}
                className={styles.sidebarIcon}
                alt={item.title}
              />
            </button>
            <span className={`${daynighttext} ${isVisible}`}>{item.title}</span>
          </Link>
        </li>
      );
    });
    return (
      <div className={`${sidebarStyles} ${daynight}`}>
        <ul>
          <li>
            <button
              className={styles.menuBtn + " " + styles.sidebarBtn}
              onClick={this.handleClick}
            >
              {this.state.open ? (
                <CloseSvg color={this.state.style ? "#04366e" : "#fff"} />
              ) : (
                <img
                  src={this.state.style ? menuIconBlue : menuIcon}
                  className={styles.sidebarIcon}
                  alt="menu"
                />
              )}
            </button>
          </li>
          {sidebarItems}
        </ul>

        <div className={styles.sidebarUpFooter}>
          <div className={styles.changeStyle}>
            <button onClick={this.changeStyle}>
              <TimeSvg />
            </button>
          </div>
        </div>
        <div className={styles.sidebarFooter}>
          <button
            className={styles.footerBtn}
            onClick={() => auth.logout(() => this.props.history.push("/"))}
          >
            <LogOutSvg
              color={this.state.style ? "#04366e" : "#fff"}
              alt="sign out"
              title="sign out"
            />
          </button>
          <span className={`${styles.versionText} ${daynighttext}`}>
            V.1.0.1
          </span>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className={styles.sidebarComponent}>{this.generateContent()}</div>
    );
  }
}

export default withRouter(Sidebar);
