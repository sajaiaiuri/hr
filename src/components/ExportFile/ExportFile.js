import React, { Component } from "react";
import styles from "./ExportFile.module.css";
import ReactToExcel from "react-html-table-to-excel";
import jsPDF from "jspdf";
import "jspdf-autotable";

class ExportFile extends Component {
  state = {
    open: false
  };

  handlePdf = () => {
    const doc = new jsPDF({
      orientation: "landscape",
      format: [1200, 1200]
    });

    doc.autoTable({
      html: "#html_to_excel",
      theme: "grid",
      styles: {
        minCellWidth: 30,
        halign: "center"
      }
    });
    doc.save("table.pdf");

    this.setState({ open: false });
  };

  render() {
    let content = this.state.open ? (
      <div className={styles.absoluteBox}>
        <button onClick={this.handlePdf} className={styles.optionBtn}>
          PDF
        </button>
        <div onClick={() => this.setState({ open: false })}>
          <ReactToExcel
            className={styles.optionBtn}
            table="html_to_excel"
            filename="candidates"
            sheet="sheet 1"
            buttonText="Excel"
          />
        </div>
      </div>
    ) : null;
    let buttonClass = this.state.open ? styles.btn : styles.btn;
    return (
      <div className={styles.container}>
        <button
          className={buttonClass}
          onClick={() => {
            this.setState({ open: !this.state.open });
          }}
        >
          <span className={styles.btnSpan}>Export</span>

          <svg
            version="1.1"
            id="Capa_1"
            x="0px"
            y="0px"
            className={styles.btnIcon}
            viewBox="0 0 512 512"
          >
            <path
              className={styles.downloadArrow}
              d="M382.56,233.376C379.968,227.648,374.272,224,368,224h-64V16c0-8.832-7.168-16-16-16h-64c-8.832,0-16,7.168-16,16v208h-64
			c-6.272,0-11.968,3.68-14.56,9.376c-2.624,5.728-1.6,12.416,2.528,17.152l112,128c3.04,3.488,7.424,5.472,12.032,5.472
			c4.608,0,8.992-2.016,12.032-5.472l112-128C384.192,245.824,385.152,239.104,382.56,233.376z"
            />
            <path d="M432,352v96H80v-96H16v128c0,17.696,14.336,32,32,32h416c17.696,0,32-14.304,32-32V352H432z" />
          </svg>
        </button>
        {content}
      </div>
    );
  }
}

export default ExportFile;
