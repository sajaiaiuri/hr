import React, { Component } from "react";
import SmartTable from "../Home/SmartTable/SmartTable";
import styles from "./Technologies.module.css";

class SingleTechnology extends Component {
  state = {
    columnHeaders: [
      { title: "Name", name: "name" },
      { title: "Phone", name: "phone" },
      { title: "Current Position", name: "position" },
      { title: "Profile", name: "profile" },
      { title: "Portfolio", name: "portfolio" },
      { title: "English", name: "english" },
      { title: "Salary Expectation", name: "salary" },
      { title: "Source", name: "source" },
      { title: "Status", name: "status" },
      { title: "Comment", name: "comment" },
      { title: "Date", name: "created_at" }
    ]
  };
  componentDidMount() {
    const technologyID = this.props.match.params.id;
    this.props.fetchTechnology(technologyID);
  }
  render() {
    return (
      <div className={styles.singleContainer}>
        <div className={styles.singleContent}>
          <SmartTable
            columnHeaders={this.state.columnHeaders}
            rows={this.props.choosenTechnology.profiles}
          />
        </div>
      </div>
    );
  }
}

export default SingleTechnology;
