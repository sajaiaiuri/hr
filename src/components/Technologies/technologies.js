import React, { Component } from "react";
import styles from "./Technologies.module.css";
import { Link } from "react-router-dom";
import PermissionGuard from "../HOC/PermissionGuard";

class Technologies extends Component {
  state = {
    open: false,
    techToBeAdded: ""
  };

  changeStatus = () => {
    this.setState(state => {
      return {
        open: !state.open
      };
    });
  };
  handleAddTechChange = e => {
    this.setState({ techToBeAdded: e.target.value });
  };
  addTech = e => {
    e.preventDefault();
    const project = {
      title: this.state.techToBeAdded,
      author_id: this.props.user.user.id
    };
    this.setState({ techToBeAdded: "" });
    this.state.techToBeAdded && this.props.createTech(project);
  };

  removeTech = e => {
    this.props.deleteTech(e.target.value);
  };

  render() {
    let generateContent = this.props.technologies.map(item => {
      return (
        <div className={styles.tech} key={item.id}>
          <PermissionGuard roles={["admin", "manager"]}>
            <div className={styles.deleteTech}>
              <button value={item.id} onClick={this.removeTech}>
                X
              </button>
            </div>
          </PermissionGuard>
          <Link
            to={`/home/technologies/${item.id}`}
            className={styles.techTitle}
          >
            #{item.title}
          </Link>
        </div>
      );
    });
    return (
      <div className={styles.scrollContainer}>
        <div className={styles.flexContainer}>
          <div className={styles.container}>
            <PermissionGuard roles={["admin", "manager"]}>
              <div className={styles.tech}>
                <div className={`${styles.techTitle} ${styles.addTech}`}>
                  <form
                    className={this.state.open ? styles.dFlex : styles.dNone}
                    onSubmit={this.addTech}
                  >
                    <input
                      type="text"
                      name="title"
                      value={this.state.techToBeAdded}
                      onChange={this.handleAddTechChange}
                    />
                    <button>Add</button>
                  </form>
                  <div
                    className={this.state.open ? styles.dNone : styles.dFlex}
                    onClick={this.changeStatus}
                  >
                    +
                  </div>
                  <div
                    className={this.state.open ? styles.cancel : styles.dNone}
                    onClick={this.changeStatus}
                  >
                    X
                  </div>
                </div>
              </div>
            </PermissionGuard>
            {generateContent}
          </div>
        </div>
      </div>
    );
  }
}

export default Technologies;
