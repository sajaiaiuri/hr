import React, { Component } from "react";
import { Bar } from "react-chartjs-2";
import styles from "./analytics.module.css";
class Analytics extends Component {
  state = {};

  chooseProject = e => {
    var projects = this.props.projects;
    var project = projects.filter(p => {
      return p.id === Number(e.target.value);
    });
    this.setState({ project: project[0] });
  };
  render() {
    if (this.state.project) {
      var profiles = this.state.project.profiles;

      var linkedin = profiles.filter(p => {
        return p.status === "wrote on linkedin";
      });
      var refused = profiles.filter(p => {
        return p.status === "refused";
      });
      var interested = profiles.filter(p => {
        return p.status === "interested";
      });
      var rejected = profiles.filter(p => {
        return p.status === "rejected";
      });
      var shortlisted = profiles.filter(p => {
        return p.status === "shortlisted";
      });
      var hired = profiles.filter(p => {
        return p.status === "hired";
      });
    }
    const data = {
      labels: [
        "wrote on linkedin",
        "refused",
        "interested",
        "rejected",
        "shortlisted",
        "hired"
      ],
      datasets: [
        {
          label: this.state.project ? this.state.project.title : "No Project",
          backgroundColor: [
            "#D86136",
            "#294672",
            "#F0C5B5",
            "#D96268",
            "#0F284F",
            "#6B0504"
          ],
          borderColor: [
            "#D86136",
            "#294672",
            "#F0C5B5",
            "#D96268",
            "#0F284F",
            "#6B0504"
          ],
          borderWidth: 2,
          hoverBackgroundColor: [
            "#D86136",
            "#294672",
            "#F0C5B5",
            "#D96268",
            "#0F284F",
            "#6B0504"
          ],
          hoverBorderColor: [
            "#D86136",
            "#294672",
            "#F0C5B5",
            "#D96268",
            "#0F284F",
            "#6B0504"
          ],
          data: [
            linkedin ? linkedin.length : 0,
            refused ? refused.length : 0,
            interested ? interested.length : 0,
            rejected ? rejected.length : 0,
            shortlisted ? shortlisted.length : 0,
            hired ? hired.length : 0
          ]
        }
      ]
    };
    // console.log(this.state.project);
    return (
      <>
        <div className={styles.totalContainer}>
          <select name="projects" onChange={this.chooseProject}>
            <option className={styles.analytics_option}>Select Project</option>
            {this.props.projects ? (
              <>
                {this.props.projects.map(project => {
                  return (
                    <option
                      key={project.id}
                      value={project.id}
                      className={styles.analytics_option}
                    >
                      {project.title}
                    </option>
                  );
                })}
              </>
            ) : null}
          </select>
          <div className={styles.flexContainer}>
            <Bar
              data={data}
              width={100}
              height={30}
              options={{
                maintainAspectRatio: true,
                scales: {
                  yAxes: [
                    {
                      ticks: {
                        beginAtZero: true
                      }
                    }
                  ]
                }
              }}
            />
          </div>
        </div>
      </>
    );
  }
}

export default Analytics;
