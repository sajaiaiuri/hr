import React, { Component } from "react";
import styles from "./ProfileList.module.css";
import Modal from "../../Modal/Modal";
import Backdrop from "../../Backdrop/Backdrop";
import SmartTable from "../SmartTable/SmartTable";
import Pagination from "../../pagination/Pagination";
import { paginate } from "../../../utils/paginate";
import ExportFile from "../../ExportFile/ExportFile";
import FilterWindow from "../FilterWindow/filterWindow";
import SVG from "./FilterIcon";
import ClearIcon from "./ClearIcon";
import CreateIcon from "./CreateIcon";
import * as validation from "../../../utils/validation";
import PermissionGuard from "../../HOC/PermissionGuard";

export default class ProfileList extends Component {
  state = {
    filtered: JSON.parse(localStorage.getItem("filterValues")) || "clear",
    creating: false,
    drawFilter: false,
    name: "",
    phone: "",
    position: "",
    profile: "",
    portfolio: "",
    comment: "",
    english: "no english",
    salary: "",
    status: "Wrote on LinkedIn",
    source: "LinkedIn",
    recommender: "me",
    black_list: false,
    pageSize: 100,
    currentPage: 1,
    columnHeaders: [
      { title: "Name", name: "name" },
      { title: "Phone", name: "phone" },
      { title: "Current Position", name: "position" },
      { title: "Profile", name: "profile" },
      { title: "Portfolio", name: "portfolio" },
      { title: "Technologies", name: "technologies" },
      { title: "English", name: "english" },
      { title: "Salary Expectation", name: "salary" },
      { title: "Source", name: "source" },
      { title: "Status", name: "status" },
      { title: "Projects", name: "projects" },
      { title: "Comment", name: "comment" },
      { title: "Date", name: "created_at" }
    ],
    technologies: [],
    projects: [],
    chosenTechnologies: [],
    chosenProjects: [],
    errors: []
  };
  setChosenTechnologies = tech => {
    this.setState(currentState => {
      return { chosenTechnologies: [...currentState.chosenTechnologies, tech] };
    });
  };
  setChosenProjects = project => {
    this.setState(currentState => {
      return { chosenTechnologies: [...currentState.chosenProjects, project] };
    });
  };
  handleMultiSelect = (category, data) => {
    if (category === "technologies") {
      this.setState({
        chosenTechnologies: data
      });
    }
    if (category === "projects") {
      this.setState({ chosenProjects: data });
    }
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value, errors: [] });
  };

  startCreateEventHandler = () => {
    this.setState({ creating: true });
  };
  modalConfirmHandler = () => {
    this.setState({ creating: false });
  };
  modalCancelHandler = () => {
    this.setState({ creating: false });
  };
  drawFilter = () => {
    this.setState({ drawFilter: !this.state.drawFilter });
  };
  closeFilter = () => {
    this.setState({ drawFilter: false });
  };
  filteredRows = a => {
    this.setState({ filtered: a });
    localStorage.setItem("filterValues", JSON.stringify(a));
  };
  cancelFilter = () => {
    this.setState({ filtered: "clear" });
    localStorage.setItem("filterValues", null);
  };

  handlePageChange = page => {
    this.setState({ currentPage: page });
  };

  createProfile = e => {
    e.preventDefault();
    this.setState({ errors: {} });
    const {
      name,
      phone,
      position,
      portfolio,
      profile,
      comment,
      english,
      salary,
      source,
      status,
      chosenProjects: projects,
      chosenTechnologies: technologies
    } = this.state;
    const userprofile = {
      author_id: this.props.user.user.id,
      status,
      name,
      phone,
      position,
      portfolio,
      profile,
      comment,
      english,
      salary,
      source,
      projects,
      technologies
    };
    validation
      .validateAddProfile(
        name,
        phone,
        position,
        profile,
        projects,
        technologies
      )
      .then(() => {
        this.props.createProfile(userprofile);
        this.setState({ creating: false });
      })
      .catch(err => {
        err.inner.map(item => {
          this.setState(currentState => {
            return {
              errors: {
                ...currentState.errors,
                [item.path]: item.message,
                creating: true
              }
            };
          });
          return 1;
        });
      });
  };

  render() {
    const profiles = paginate(
      this.state.filtered === "clear"
        ? this.props.profiles
        : this.state.filtered,
      this.state.currentPage,
      this.state.pageSize
    );
    return (
      <div className={styles.container}>
        <div className={styles.content}>
          <div className={styles.buttonContainer}>
            <PermissionGuard roles={["admin", "manager"]}>
              <div>
                <button
                  className={`${styles.addcandidate} ${styles.btn}`}
                  onClick={this.startCreateEventHandler}
                >
                  <span>Create Profile</span> &nbsp;&nbsp;
                  <CreateIcon />
                </button>
              </div>
            </PermissionGuard>
            <div className={styles.profilesListBtnRight}>
              <ExportFile />
              <button
                onClick={this.drawFilter}
                className={`${styles.profilesListBtn}  ${styles.btn}`}
                disabled={!this.props.profiles.length}
              >
                <span>Filter</span>
                <SVG active={this.state.filtered !== "clear"} />
              </button>
              <button
                onClick={this.cancelFilter}
                className={`${styles.profilesListBtn} ${styles.btn}`}
                disabled={!this.props.profiles.length}
              >
                <span>Clear Filter</span>
                <ClearIcon />
              </button>
            </div>
          </div>
          <div className={styles.profilesTable}>
            <SmartTable
              columnHeaders={this.state.columnHeaders}
              rows={profiles}
            />
          </div>
          <Pagination
            itemsCount={
              this.state.filtered === "clear"
                ? this.props.profiles.length
                : this.state.filtered.length
            }
            pageSize={this.state.pageSize}
            currentPage={this.state.currentPage}
            onPageChange={this.handlePageChange}
          />
        </div>
        {this.state.drawFilter && <Backdrop />}
        {this.state.drawFilter && (
          <FilterWindow
            currentRows={this.props.profiles}
            projects={this.props.projects}
            technologies={this.props.technologies}
            filteredRows={this.filteredRows}
            closeFilter={this.closeFilter}
          />
        )}
        {this.state.creating && <Backdrop />}
        {this.state.creating && (
          <Modal
            title="Add Candidate"
            submitButtontext="add"
            canCancel
            canConfirm
            onCancel={this.modalCancelHandler}
            onConfirm={this.createProfile}
            fields={[
              {
                name: "name",
                type: "text",
                label: "Name",
                value: this.state.name
              },
              {
                name: "phone",
                type: "text",
                label: "Phone",
                value: this.state.phone
              },
              {
                name: "position",
                type: "text",
                label: "Position",
                value: this.state.position
              },
              {
                name: "portfolio",
                type: "text",
                label: "Portfolio",
                value: this.state.portfolio
              },
              {
                name: "comment",
                type: "text",
                label: "Comment",
                value: this.state.comment
              },
              {
                name: "salary",
                type: "text",
                label: "Salary Expectation",
                value: this.state.salary
              },
              {
                name: "profile",
                type: "text",
                label: "Profile",
                value: this.state.profile
              },
              {
                name: "recommender",
                type: "text",
                label: "Recommender",
                value: this.state.recommender
              },
              {
                name: "english",
                type: "select",
                label: "English",
                options: [
                  { value: "no english", label: "no english" },
                  { value: "good", label: "good" },
                  { value: "fluent", label: "fluent" }
                ]
              },
              {
                name: "status",
                type: "select",
                label: "Status",
                options: [
                  { value: "wrote on linkedin", label: "Wrote on LinkedIn" },
                  { value: "refused", label: "Refused" },
                  { value: "interested", label: "Interested" },
                  { value: "rejected", label: "Rejected" },
                  { value: "shortlisted", label: "Shortlisted" },
                  { value: "hired", label: "Hired" },
                  { value: "black_list", label: "Blacklisted" }
                ]
              },
              {
                name: "source",
                type: "select",
                label: "Source",
                options: [
                  { value: "LinkedIn", label: "LinkedIn" },
                  { value: "Refference", label: "Refference" },
                  { value: "Job Post", label: "Job Post" }
                ]
              },

              {
                name: "technologies",
                type: "multiSelect",
                label: "Technologies",
                options: this.props.technologies.map(item => {
                  return { value: item.id, label: item.title };
                }),
                selected: this.state.chosenTechnologies
              },
              {
                name: "projects",
                type: "multiSelect",
                label: "Projects",
                options: this.props.projects.map(item => {
                  return { value: item.id, label: item.title };
                }),
                selected: this.state.chosenProjects
              }
            ]}
            onChange={this.handleChange}
            handleMultiSelect={this.handleMultiSelect}
            errors={this.state.errors}
            candidate={{}}
            setTechnologies={this.setChosenTechnologies}
            setProjects={this.setChosenProjects}
          />
        )}
      </div>
    );
  }
}
