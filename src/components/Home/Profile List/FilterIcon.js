import React from "react";
import styles from "./ProfileList.module.css";

const SVG = props => (
  <svg
    version="1.1"
    id="Capa_1"
    xmlns="http://www.w3.org/2000/svg"
    x="0px"
    y="0px"
    viewBox="0 0 54 54"
    width="20px"
    className={props.active ? styles.activeFilterIcon : styles.filterIcon}
  >
    <g>
      <path
        style={{ fill: "#cccccc" }}
        d="M6,0C5.448,0,5,0.447,5,1v40c0,0.553,0.448,1,1,1s1-0.447,1-1V1C7,0.447,6.552,0,6,0z"
      />
      <path
        className={styles.filterIconRount}
        d="M48,12c-0.552,0-1,0.447-1,1v40c0,0.553,0.448,1,1,1s1-0.447,1-1V13C49,12.447,48.552,12,48,12z"
      />
      <path
        style={{ fill: "#cccccc" }}
        d="M27,0c-0.552,0-1,0.447-1,1v19c0,0.553,0.448,1,1,1s1-0.447,1-1V1C28,0.447,27.552,0,27,0z"
      />
      <path
        className={styles.filterIconRount}
        d="M27,32c-0.552,0-1,0.447-1,1v20c0,0.553,0.448,1,1,1s1-0.447,1-1V33C28,32.447,27.552,32,27,32z"
      />
    </g>
    <circle
      className={`${styles.filterIconRount} ${styles.filterIconRount}`}
      cx="6"
      cy="47"
      r="6"
    />
    <circle
      className={`${styles.filterIconRount} ${styles.filterIconRount}`}
      cx="26.793"
      cy="26.793"
      r="6.207"
    />
    <circle
      className={`${styles.filterIconRount} ${styles.filterIconRount}`}
      cx="48"
      cy="7"
      r="6"
    />
  </svg>
);

export default SVG;
