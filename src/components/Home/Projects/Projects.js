import React, { Component } from "react";
import styles from "./Projects.module.css";
import { Link } from "react-router-dom";
import PermissionGuard from "../../HOC/PermissionGuard";

export default class Home extends Component {
  state = {
    open: false,
    projectToBeAdded: ""
  };

  removeProject = e => {
    this.props.deleteProject(e.target.value);
  };

  changeStatus = () => {
    this.setState(state => {
      return {
        open: !state.open
      };
    });
  };
  handleAddprojectChange = e => {
    this.setState({ projectToBeAdded: e.target.value });
  };
  addProject = e => {
    e.preventDefault();
    const project = {
      title: this.state.projectToBeAdded,
      author_id: this.props.user.user.id
    };
    this.setState({ projectToBeAdded: "" });
    this.state.projectToBeAdded && this.props.createProject(project);
  };
  render() {
    let generateContent = this.props.projects.map(item => {
      return (
        <div className={styles.project} key={item.id}>
          <PermissionGuard roles={["admin", "manager"]}>
            <div className={styles.deleteProject}>
              <button value={item.id} onClick={this.removeProject}>
                X
              </button>
            </div>
          </PermissionGuard>
          <Link
            to={`/home/projects/${item.id}`}
            className={styles.projectTitle}
          >
            {item.title}
          </Link>
        </div>
      );
    });
    return (
      <div className={styles.scrollContainer}>
        <div className={styles.flexContainer}>
          <div className={styles.container}>
            <PermissionGuard roles={["admin", "manager"]}>
              <div className={styles.project}>
                <div className={`${styles.projectTitle} ${styles.addProject}`}>
                  <form
                    className={this.state.open ? styles.dFlex : styles.dNone}
                    onSubmit={this.addProject}
                  >
                    <input
                      type="text"
                      name="title"
                      value={this.state.projectToBeAdded}
                      onChange={this.handleAddprojectChange}
                    />
                    <button>Add</button>
                  </form>
                  <div
                    className={this.state.open ? styles.dNone : styles.dFlex}
                    onClick={this.changeStatus}
                  >
                    +
                  </div>
                  <div
                    className={this.state.open ? styles.cancel : styles.dNone}
                    onClick={this.changeStatus}
                  >
                    X
                  </div>
                </div>
              </div>
            </PermissionGuard>
            {generateContent}
          </div>
        </div>
      </div>
    );
  }
}
