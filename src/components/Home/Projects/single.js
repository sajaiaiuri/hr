import React, { Component } from "react";
import SmartTable from "../../Home/SmartTable/SmartTable";
import styles from "./Projects.module.css";

class SingleProject extends Component {
  state = {
    columnHeaders: [
      { title: "Name", name: "name" },
      { title: "Phone", name: "phone" },
      { title: "Current Position", name: "position" },
      { title: "Profile", name: "profile" },
      { title: "Portfolio", name: "portfolio" },
      { title: "English", name: "english" },
      { title: "Salary Expectation", name: "salary" },
      { title: "Source", name: "source" },
      { title: "Status", name: "status" },
      { title: "Comment", name: "comment" },
      { title: "Date", name: "created_at" }
    ]
  };
  componentDidMount() {
    const projectID = this.props.match.params.id;
    this.props.getProject(projectID);
  }
  render() {
    return (
      <div className={styles.singleContainer}>
        <div className={styles.singleContent}>
          <SmartTable
            columnHeaders={this.state.columnHeaders}
            rows={this.props.choosenProject.profiles}
          />
        </div>
      </div>
    );
  }
}

export default SingleProject;
