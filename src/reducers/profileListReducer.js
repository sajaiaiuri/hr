import {
  DELETE_PROFILE,
  ADD_PROFILE,
  ADD_PROJECT,
  ADD_TECH,
  CHANGE_PROFILE,
  FETCH_PROFILES,
  FILTER_PROFILES,
  FETCH_PROJECTS,
  FETCH_TECHNOLOGIES,
  FETCH_PROJECT,
  FETCH_TECHNOLOGY,
  DELETE_PROJECT,
  DELETE_TECH,
  FETCH_USERS,
  ADD_USER,
  DELETE_USER,
  UPDATE_USER,
  UPDATE_PROFILE,
  LOGIN,
  KEEP_ME_SIGNED_IN,
  SET_USER
} from "../constants/profileListConstants";
const initialState = {
  User: {},
  profiles: [],
  technologies: [],
  projects: [],
  choosenTechnology: {},
  choosenProject: {},
  error: null,
  users: [],
  keepMeSignedIn: true
};

const profileListReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PROFILES:
      return {
        ...state,
        profiles: action.payload.profiles
      };
    case ADD_PROFILE:
      return {
        ...state,
        profiles: [
          ...state.profiles,
          {
            ...action.payload.profile,
            technologies: [...action.payload.technologies],
            projects: [...action.payload.projects]
          }
        ]
      };
    case ADD_PROJECT:
      return {
        ...state,
        projects: [
          ...state.projects,
          {
            ...action.payload
          }
        ]
      };
    case DELETE_PROJECT:
      const filtered = state.projects.filter(
        item => item.id !== Number(action.payload)
      );

      return {
        ...state,
        projects: filtered
      };
    case DELETE_TECH:
      const filteredtech = state.technologies.filter(
        item => item.id !== Number(action.payload)
      );

      return {
        ...state,
        technologies: filteredtech
      };

    case ADD_TECH:
      return {
        ...state,
        technologies: [
          ...state.technologies,
          {
            ...action.payload
          }
        ]
      };
    case ADD_USER:
      return {
        ...state,
        users: [...state.users, { ...action.payload, role: "viewer" }]
      };

    case UPDATE_USER:
      return {
        ...state,
        users: [
          ...state.users.filter(x => x.id < action.payload.id),
          action.payload,
          ...state.users.filter(x => x.id > action.payload.id)
        ]
      };
    case DELETE_USER:
      const filteredUsers = state.users.filter(x => x.id !== action.payload);

      return {
        ...state,
        users: filteredUsers
      };
    case DELETE_PROFILE:
      return {
        ...state,
        profiles: [
          ...state.profiles.filter(item => item.id < action.payload),
          ...state.profiles.filter(item => item.id > action.payload)
        ]
      };
    case UPDATE_PROFILE:
      return {
        ...state,
        profiles: [
          ...state.profiles.filter(
            item => item.id < action.payload.data.profile.id
          ),
          {
            ...action.payload.data.profile,
            projects: action.payload.data.projects,
            technologies: action.payload.data.technologies
          },
          ...state.profiles.filter(
            item => item.id > action.payload.data.profile.id
          )
        ]
      };
    case FILTER_PROFILES:
      return {
        ...state,
        profiles: action.profiles
      };
    case FETCH_TECHNOLOGIES:
      return {
        ...state,
        technologies: action.payload.technologies
      };
    case FETCH_TECHNOLOGY:
      return {
        ...state,
        choosenTechnology: {
          ...action.payload.technology,
          profiles: action.payload.profiles
        }
      };
    case FETCH_PROJECTS:
      return {
        ...state,
        projects: action.payload.projects
      };
    case FETCH_PROJECT:
      return {
        ...state,
        choosenProject: {
          ...action.payload.project,
          profiles: action.payload.profiles
        }
      };
    case FETCH_USERS:
      return {
        ...state,
        users: action.payload
      };
    case CHANGE_PROFILE:
      return {
        ...state,
        profiles: [...state.profiles, action.payload.profile]
      };
    case LOGIN:
      return {
        ...state,
        User: action.payload
      };
    case KEEP_ME_SIGNED_IN:
      return {
        ...state,
        keepMeSignedIn: !state.keepMeSignedIn
      };
    case SET_USER:
      return {
        ...state,
        User: action.User
      };
    default:
      return state;
  }
};

export default profileListReducer;
